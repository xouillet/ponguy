#! /bin/bash

SOURCE=$( dirname ${BASH_SOURCE[0]} )
BUILD_DIR=${SOURCE}/.build/

DEVICE=/dev/ttyACM0
if [ -n "$1" ]; then
    DEVICE=$1
fi

echo "Uploading ponguy to ${DEVICE}..."

cd ${SOURCE}

mkdir -p ${BUILD_DIR}
mkdir -p ${BUILD_DIR}/lib
cp -a ./submodules/logging.mpy ./submodules/typing.mpy ./submodules/tinyweb/tinyweb/ ${BUILD_DIR}/lib
cp -a ./main.py ./ponguy ${BUILD_DIR}
mkdir -p ${BUILD_DIR}/webui
cp -a ./webui/dist/* ${BUILD_DIR}/webui

cd ${BUILD_DIR}
mpremote connect $DEVICE fs cp -r ./main.py ./ponguy/*.py ./lib/ ./webui/ : + reset
