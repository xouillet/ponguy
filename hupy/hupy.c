#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bsp/board.h"
#include "tusb.h"
#include "tusb_config.h"

#include "ws2812.pio.h"

#include "ponguy.h"
#include "usb.h"

#define WS2812_PIN 16

void led_init();
void led_blinking_task(void);

int main(void) {
  board_init();

  tuh_init(BOARD_TUH_RHPORT);
  led_init();
  ponguy_init();

  printf("Hupy is ready !!!\r\n");

  while (1) {
    tuh_task();
    led_blinking_task();
    hupy_task();
  }

  return 0;
}

/*
 * LED Blinking
 */
void led_init() {
  // init led
  PIO pio = pio0;
  int sm = 0;
  uint offset = pio_add_program(pio, &ws2812_program);
  ws2812_program_init(pio, sm, offset, WS2812_PIN, 800000, false);
}

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
  return (((uint32_t)(r) << 8) | ((uint32_t)(g) << 16) | (uint32_t)(b)) << 8;
}

void led_blinking_task(void) {
  const uint32_t interval_ms = 1000;
  static uint32_t start_ms = 0;

  static int led_state = 0;

  // Blink every interval ms
  if (board_millis() - start_ms < interval_ms)
    return; // not enough time
  start_ms += interval_ms;

  pio_sm_put_blocking(pio0, 0,
                      urgb_u32((led_state % 3 == 0) ? 0x33 : 0,
                               (led_state % 3 == 1) ? 0x33 : 0,
                               (led_state % 3 == 2) ? 0x33 : 0));
  led_state++;
}