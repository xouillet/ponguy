#ifndef HUPY_XINPUT_PKT
#define HUPY_XINPUT_PKT

#include <stdio.h>
#include <stdlib.h>

#define XINPUT_ID_VENDOR 0x045E
#define XINPUT_ID_PRODUCT 0x028E

typedef struct TU_ATTR_PACKED {
  uint8_t rid;
  uint8_t rsize;

  uint8_t dpad_up : 1;
  uint8_t dpad_down : 1;
  uint8_t dpad_left : 1;
  uint8_t dpad_right : 1;
  uint8_t start : 1;
  uint8_t back : 1;
  uint8_t ls : 1;
  uint8_t rs : 1;

  uint8_t lb : 1;
  uint8_t rb : 1;
  uint8_t xbox : 1;
  uint8_t _unused : 1;
  uint8_t a : 1;
  uint8_t b : 1;
  uint8_t x : 1;
  uint8_t y : 1;

  uint8_t lt;
  uint8_t rt;
  int16_t lx;
  int16_t ly;
  int16_t rx;
  int16_t ry;
  uint8_t reserved_1[6];
} xinput_report_t;

#endif