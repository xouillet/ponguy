/* Taken from
 * https://github.com/fluffymadness/tinyusb-xinput/blob/master/descriptor_xinput.h
 */

#ifndef DESCRIPTOR_XINPUT
#define DESCRIPTOR_XINPUT

#include "./usb.h"
#include "./xinput_report.h"
#include "common/tusb_common.h"
#include "device/usbd.h"
#include "pico/stdlib.h"
#include "tusb.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline bool is_xinput(uint8_t dev_addr) {
  uint16_t vid, pid;
  tuh_vid_pid_get(dev_addr, &vid, &pid);

  return ((vid == XINPUT_ID_VENDOR && pid == XINPUT_ID_PRODUCT));
}

void xinput_init(uint8_t dev_addr, tusb_desc_interface_t const *itf_desc,
                 uint16_t drv_len);
void process_xinput_report(tuh_xfer_t *xfer);
void xinput_event(struct event event);

#endif