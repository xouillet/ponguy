#ifndef HUPY_HID_H
#define HUPY_HID_H

#include "tusb.h"

#define MAX_PLAYERS 2
#define MAX_QUEUED_EVENTS 20

enum driver { NOT_CONNECTED = 0, XINPUT, BUZZ };

struct player {
  enum driver driver;
  uint8_t dev_addr;

  uint8_t dev_instance;

  uint8_t xinput_endpoint_in;
  uint8_t xinput_endpoint_out;
};

enum event_type { NO_EVENT = 0, RESET, LED, RUMBLE };
struct event {
  enum event_type event_type;
  uint8_t player_id;
  uint16_t data;
};

extern struct player players[MAX_PLAYERS];
extern struct event events[MAX_QUEUED_EVENTS];

int8_t new_player(uint8_t dev_addr, enum driver driver, uint8_t preferred);
int8_t find_player(uint8_t dev_addr);

static void process_hid_report(uint8_t dev_addr, uint8_t instance,
                               uint8_t const *report, uint16_t len);

void bare_init_cb(tuh_xfer_t *xfer);
uint16_t bare_count_interface_total_len(tusb_desc_interface_t const *desc_itf,
                                        uint8_t itf_count, uint16_t max_len);

void hupy_event(struct event event);
void hupy_task(void);

#endif