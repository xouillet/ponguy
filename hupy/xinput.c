#include "./xinput.h"
#include "./usb.h"
#include "bsp/board.h"
#include "ponguy.h"

static uint8_t xinput_dev_addr;
static uint8_t xinput_instance;

void xinput_init(uint8_t dev_addr, tusb_desc_interface_t const *itf_desc,
                 uint16_t drv_len) {

  if (!is_xinput(dev_addr))
    return;

  if (itf_desc->bInterfaceNumber != 0) {
    return;
  }
  printf("Xinput device detected !\r\n");
  tusb_desc_device_t desc_device;
  tuh_descriptor_get_device(dev_addr, &desc_device, sizeof(desc_device), NULL,
                            0);
  int8_t player = new_player(dev_addr, XINPUT,
                             (desc_device.bcdDevice - 0x572) % MAX_PLAYERS);

  uint8_t const *p_desc = tu_desc_next(itf_desc);
  uint8_t found_endpoints = 0;
  while ((found_endpoints < itf_desc->bNumEndpoints)) {
    tusb_desc_endpoint_t const *desc_ep = (tusb_desc_endpoint_t const *)p_desc;
    if (TUSB_DESC_ENDPOINT == tu_desc_type(desc_ep)) {
      if (!tuh_edpt_open(dev_addr, desc_ep)) {
        printf("Error opening endpoint\r\n");
        return;
      }

      if (tu_edpt_dir(desc_ep->bEndpointAddress) == TUSB_DIR_IN) {
        players[player].xinput_endpoint_in = desc_ep->bEndpointAddress;
      } else {
        players[player].xinput_endpoint_out = desc_ep->bEndpointAddress;
      }
      found_endpoints += 1;
    }
    p_desc = tu_desc_next(p_desc);
  }

  uint8_t buf_in[64];
  tuh_xfer_t xfer_in = {
      .daddr = dev_addr,
      .ep_addr = players[player].xinput_endpoint_in,
      .buflen = sizeof(buf_in),
      .buffer = buf_in,
      .complete_cb = process_xinput_report,
      .user_data =
          (uintptr_t)buf_in, // since buffer is not available in callback,
                             // use user data to store the buffer
  };
  tuh_edpt_xfer(&xfer_in);
}

void process_xinput_report(tuh_xfer_t *xfer) {
  uint8_t *buf = (uint8_t *)xfer->user_data;
  xinput_report_t *xinput_data = (xinput_report_t *)buf;

  int8_t player = find_player(xfer->daddr);
  if (player >= 0 && (xfer->result == XFER_RESULT_SUCCESS)) {
    ponguy_btn(player, xinput_data->a);
  }

  // wait for next input
  xfer->buflen = 64;
  xfer->buffer = buf;
  tuh_edpt_xfer(xfer);
}

void xinput_event(struct event event) {
  if (event.event_type == LED) {
    uint8_t set_led[3] = {0x01, 0x03, (uint8_t)event.data};
    tuh_xfer_t xfer_out = {.daddr = players[event.player_id].dev_addr,
                           .ep_addr =
                               players[event.player_id].xinput_endpoint_out,
                           .buflen = sizeof(set_led),
                           .buffer = set_led,
                           .complete_cb = NULL};
    tuh_edpt_xfer(&xfer_out);
  } else if (event.event_type == RUMBLE) {
    /* Handle rumble */
    uint8_t rumble[8] = {0x00, 0x08, 0x00, event.data >> 8, event.data & 0xff,
                         0x00, 0x00, 0x00};
    tuh_xfer_t xfer_rumble = {.daddr = players[event.player_id].dev_addr,
                              .ep_addr =
                                  players[event.player_id].xinput_endpoint_out,
                              .buflen = sizeof(rumble),
                              .buffer = rumble,
                              .complete_cb = NULL};
    tuh_edpt_xfer(&xfer_rumble);
  }
}
