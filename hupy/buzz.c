#include "./buzz.h"
#include "./usb.h"

#include "bsp/board.h"

#include "ponguy.h"

#define NB_BUZZ_PLAYERS 2 /* Up to 4 but max players is 2 now */

void buzz_init(uint8_t dev_addr, uint8_t instance) {
  if (!is_buzz(dev_addr))
    return;

  /* Buzz has multiple player on one device */
  for (int i = 0; i < NB_BUZZ_PLAYERS; i++) {
    int8_t player = new_player(dev_addr, BUZZ, 0);
    players[player].dev_instance = instance;
  }
}

void process_buzz_report(uint8_t dev_addr, uint8_t instance,
                         uint8_t const *report, uint16_t len) {
  /* Map buzz report struct */
  buzz_report_t buzz_report;
  memcpy(&buzz_report, report, sizeof(buzz_report));

  int8_t player = find_player(dev_addr);

  ponguy_btn(player, buzz_report.buzz_1 || buzz_report.buzz_3);
  ponguy_btn(player + 1, buzz_report.buzz_2 || buzz_report.buzz_4);
}

void buzz_event(struct event event) {
  if (event.event_type == LED) {
    static buzz_led_report_t led_report = {0};
    if (event.data > 0) {
      *(&led_report.led_1 + event.player_id) = 0xff;
      if (!tuh_hid_set_report(players[event.player_id].dev_addr,
                              players[event.player_id].dev_instance, 0,
                              HID_REPORT_TYPE_OUTPUT, &led_report,
                              sizeof(led_report))) {
        printf("Error in send hid :(\r\n");
      }
    }
  }
}
