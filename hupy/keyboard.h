#ifndef HUPY_KEYBOARD_H
#define HUPY_KEYBOARD_H

#include "tusb.h"

static uint8_t const keycode2ascii[128][2] = {HID_KEYCODE_TO_ASCII};

void process_kbd_report(hid_keyboard_report_t const *report);

#endif