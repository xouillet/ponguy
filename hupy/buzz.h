#ifndef HUPY_BUZZ_H
#define HUPY_BUZZ_H

#include "./usb.h"
#include "tusb.h"

typedef struct TU_ATTR_PACKED {
  uint16_t hdr;

  uint8_t buzz_1 : 1;
  uint8_t yellow_1 : 1;
  uint8_t green_1 : 1;
  uint8_t orange_1 : 1;
  uint8_t blue_1 : 1;

  uint8_t buzz_2 : 1;
  uint8_t yellow_2 : 1;
  uint8_t green_2 : 1;
  uint8_t orange_2 : 1;
  uint8_t blue_2 : 1;

  uint8_t buzz_3 : 1;
  uint8_t yellow_3 : 1;
  uint8_t green_3 : 1;
  uint8_t orange_3 : 1;
  uint8_t blue_3 : 1;

  uint8_t buzz_4 : 1;
  uint8_t yellow_4 : 1;
  uint8_t green_4 : 1;
  uint8_t orange_4 : 1;
  uint8_t blue_4 : 1;

} buzz_report_t;

typedef struct TU_ATTR_PACKED {
  int8_t hdr;
  int8_t led_1;
  int8_t led_2;
  int8_t led_3;
  int8_t led_4;
  int8_t footer[2];
} buzz_led_report_t;

extern bool buzz_connected;
static inline bool is_buzz(uint8_t dev_addr) {
  uint16_t vid, pid;
  tuh_vid_pid_get(dev_addr, &vid, &pid);

  return ((vid == 0x054c && pid == 0x1000));
}
void buzz_init(uint8_t dev_addr, uint8_t instance);
void process_buzz_report(uint8_t dev_addr, uint8_t instance,
                         uint8_t const *report, uint16_t len);
void buzz_event(struct event event);

#endif