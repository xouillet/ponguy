#include "./usb.h"

#include "./buzz.h"
#include "./keyboard.h"
#include "./ponguy.h"
#include "./xinput.h"

/*
 * Global USB callbacks
 */

struct player players[MAX_PLAYERS];
struct event events[MAX_QUEUED_EVENTS] = {0};

void tuh_mount_cb(uint8_t dev_addr) {
  uint16_t vid, pid;
  tuh_vid_pid_get(dev_addr, &vid, &pid);
  printf("New device [%04x:%04x] address = %d\r\n", vid, pid, dev_addr);

  if (is_xinput(dev_addr)) {
    tusb_desc_device_t desc_device;
    tuh_descriptor_get_device(dev_addr, &desc_device, sizeof(desc_device),
                              bare_init_cb, 0);
  }
}

void tuh_umount_cb(uint8_t dev_addr) {
  for (int i = 0; i < MAX_PLAYERS; i++) {
    if (players[i].dev_addr == dev_addr) {
      players[i].driver = NOT_CONNECTED;
    }
  }
  printf("Device disconnected: %d\r\n", dev_addr);
}

int8_t new_player(uint8_t dev_addr, enum driver driver, uint8_t preferred) {
  int8_t player = -1;
  if (players[preferred].driver != NOT_CONNECTED) {
    /* Find first free device slot */
    int i;
    for (i = 0; i < MAX_PLAYERS; i++) {
      if (players[i].driver == NOT_CONNECTED) {
        player = i;
        break;
      }
    }
    if (player == -1) {
      printf("Too many players :(\r\n");
      return player;
    }
  } else {
    player = preferred;
  }

  players[player].dev_addr = dev_addr;
  players[player].driver = driver;
  ponguy_new(player);

  printf("New player %d with driver %d\n", player, driver);
  return player;
}

int8_t find_player(uint8_t dev_addr) {
  int i;
  for (i = 0; i < MAX_PLAYERS; i++) {
    if (players[i].dev_addr == dev_addr) {
      return i;
    }
  }
  return -1;
}

/*
 * HID
 */

void tuh_hid_mount_cb(uint8_t dev_addr, uint8_t instance,
                      uint8_t const *desc_report, uint16_t desc_len) {

  int player = find_player(dev_addr);
  if (player < 0)
    return;

  buzz_init(dev_addr, instance);

  if (!tuh_hid_receive_report(dev_addr, instance)) {
    printf("Error: cannot request to receive report\r\n");
  }
}

void tuh_hid_report_received_cb(uint8_t dev_addr, uint8_t instance,
                                uint8_t const *report, uint16_t len) {
  uint8_t const itf_protocol = tuh_hid_interface_protocol(dev_addr, instance);
  switch (itf_protocol) {
  case HID_ITF_PROTOCOL_KEYBOARD:
    process_kbd_report((hid_keyboard_report_t const *)report);
    break;

  case HID_ITF_PROTOCOL_NONE:
    process_hid_report(dev_addr, instance, report, len);
    break;
  }

  // continue to request to receive report
  if (!tuh_hid_receive_report(dev_addr, instance)) {
    printf("Error: cannot request to receive report\r\n");
  }
}

/*
 * Bare API (needed for xinput)
 */
static void init_bare(uint8_t dev_addr, tusb_desc_interface_t const *itf_desc,
                      uint16_t drv_len);
void bare_init_cb(tuh_xfer_t *xfer) {
  uint8_t const daddr = xfer->daddr;
  uint16_t buf[128];
  tusb_desc_configuration_t *desc_cfg = (tusb_desc_configuration_t *)&buf;
  if (XFER_RESULT_SUCCESS ==
      tuh_descriptor_get_configuration_sync(daddr, 0, &buf, sizeof(buf))) {

    uint8_t const *desc_end =
        ((uint8_t const *)desc_cfg) + tu_le16toh(desc_cfg->wTotalLength);
    uint8_t const *p_desc = tu_desc_next(desc_cfg);

    // parse each interfaces
    while (p_desc < desc_end) {
      uint8_t assoc_itf_count = 1;
      // Class will always starts with Interface Association (if any) and then
      // Interface descriptor
      if (TUSB_DESC_INTERFACE_ASSOCIATION == tu_desc_type(p_desc)) {
        tusb_desc_interface_assoc_t const *desc_iad =
            (tusb_desc_interface_assoc_t const *)p_desc;
        assoc_itf_count = desc_iad->bInterfaceCount;

        p_desc = tu_desc_next(p_desc); // next to Interface
      }

      // must be interface from now
      if (TUSB_DESC_INTERFACE != tu_desc_type(p_desc)) {
        printf("Expecting interface\r\n");
        return;
      }
      tusb_desc_interface_t const *desc_itf =
          (tusb_desc_interface_t const *)p_desc;

      uint16_t const drv_len = bare_count_interface_total_len(
          desc_itf, assoc_itf_count, (uint16_t)(desc_end - p_desc));

      // probably corrupted descriptor
      if (drv_len < sizeof(tusb_desc_interface_t)) {
        printf("Corrupted desc\r\n");
        return;
      }

      // Everything looks good, send it to the driver !
      init_bare(daddr, desc_itf, drv_len);

      // next Interface or IAD descriptor
      p_desc += drv_len;
    }
  }
}

uint16_t bare_count_interface_total_len(tusb_desc_interface_t const *desc_itf,
                                        uint8_t itf_count, uint16_t max_len) {
  uint8_t const *p_desc = (uint8_t const *)desc_itf;
  uint16_t len = 0;

  while (itf_count--) {
    // Next on interface desc
    len += tu_desc_len(desc_itf);
    p_desc = tu_desc_next(p_desc);

    while (len < max_len) {
      // return on IAD regardless of itf count
      if (tu_desc_type(p_desc) == TUSB_DESC_INTERFACE_ASSOCIATION)
        return len;

      if ((tu_desc_type(p_desc) == TUSB_DESC_INTERFACE) &&
          ((tusb_desc_interface_t const *)p_desc)->bAlternateSetting == 0) {
        break;
      }

      len += tu_desc_len(p_desc);
      p_desc = tu_desc_next(p_desc);
    }
  }
  return len;
}

/*
 * Process callbacks (call each subdriver)
 */

static void process_hid_report(uint8_t dev_addr, uint8_t instance,
                               uint8_t const *report, uint16_t len) {
  if (is_buzz(dev_addr)) {
    process_buzz_report(dev_addr, instance, report, len);
  } else {
    printf("Received report from unknown device :(\r\n");
  }
}

static void init_bare(uint8_t dev_addr, tusb_desc_interface_t const *itf_desc,
                      uint16_t drv_len) {

  xinput_init(dev_addr, itf_desc, drv_len);
}

struct event *events_r = events;
struct event *events_w = events;

void hupy_event(struct event event) {
  *events_w = event;
  if (events_w++ >= events + MAX_QUEUED_EVENTS)
    events_w = events;
}

void hupy_task(void) {
  const uint32_t interval_ms = 10;
  static uint32_t start_ms = 0;
  if (board_millis() - start_ms < interval_ms)
    return; // not enough time
  start_ms += interval_ms;

  if (events_r != events_w) {
    struct event event = *events_r;
    printf("EVENT !! %d\r\n", event.event_type);
    if (players[event.player_id].driver == XINPUT) {
      xinput_event(event);
    } else if (players[event.player_id].driver == BUZZ) {
      buzz_event(event);
    }
    if (events_r++ >= events + MAX_QUEUED_EVENTS)
      events_r = events;
  }
}