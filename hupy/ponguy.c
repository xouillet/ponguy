#include "ponguy.h"

#include "hardware/structs/uart.h"
#include "hardware/uart.h"
#include "pico/bootrom.h"
#include "pico/stdlib.h"
#include <stdio.h>

#include "usb.h"

#define UART_ID uart1

#define UART_TX_PIN 4
#define UART_RX_PIN 5

#define BAUD_RATE 115200
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY UART_PARITY_NONE

void ponguy_rx() {
  while (uart_is_readable(UART_ID)) {
    uint8_t ch = uart_getc(UART_ID);
    if (ch == 'L') {
      uint8_t player = uart_getc(UART_ID) - 48;
      uint8_t state = uart_getc(UART_ID) - 48;
      if (player >= 0 && player < 2) {
        struct event event = {
            .player_id = player, .event_type = LED, .data = state};
        hupy_event(event);
      }
    }
    if (ch == 'R') {
      uint8_t player = uart_getc(UART_ID) - 48;
      uint8_t rumble = uart_getc(UART_ID) - 48;
      if (player >= 0 && player < 2) {
        struct event event = {.player_id = player,
                              .event_type = RUMBLE,
                              .data = (rumble == 1) ? 0xffff : 0};
        hupy_event(event);
      }
    }
    if (ch == 'Z') {
      reset_usb_boot(0, 0);
    }
  }
}

void ponguy_new(uint8_t player) {
  uart_putc(UART_ID, 'I');
  uart_putc(UART_ID, player + 48);

  uart_putc(UART_ID, '\n');
}

void ponguy_init(void) {
  uart_init(UART_ID, BAUD_RATE);

  gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
  gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);

  uart_set_hw_flow(UART_ID, false, false);
  uart_set_format(UART_ID, DATA_BITS, STOP_BITS, PARITY);
  uart_set_fifo_enabled(UART_ID, false);

  irq_set_exclusive_handler(UART1_IRQ, ponguy_rx);
  irq_set_enabled(UART1_IRQ, true);
  uart_set_irq_enables(UART_ID, true, false);
}

void ponguy_btn(uint8_t player, bool state) {
  printf("BTN %d %d\r\n", player, state);
  uart_putc(UART_ID, 'B');
  uart_putc(UART_ID, player + 48);
  uart_putc(UART_ID, state + 48);
  uart_putc(UART_ID, '\n');
}