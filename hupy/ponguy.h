#ifndef HUPY_PONGUY_H
#define HUPY_PONGUY_H

#include "bsp/board.h"

extern bool ponguy_leds[2];

void ponguy_init(void);
void ponguy_new(uint8_t player);
void ponguy_btn(uint8_t player, bool state);
void ponguy_task(void);

#endif