from machine import UART, Timer

from ponguy.config import config
from ponguy.player import Player


class Music:
    NO_MUSIC = 0
    START = 1
    PLAYER_1 = 2
    PLAYER_2 = 3
    BONUS = 4
    BALL_LOST = 5
    GAME_WIN = 6


class Chupi:
    def __init__(self, players):
        self.uart = UART(config.chupi_uart_id, 115200)  # init with given baudrate
        self.uart.init(
            115200, bits=8, parity=None, stop=1, tx=config.chupi_uart_tx, rx=config.chupi_uart_rx
        )  # init with given parameters

        self.players = players
        # Use only even id for Timer on ESP32-C3 for some reasons..
        self.tims = [Timer(2 * id) for id, _ in enumerate(self.players)]

    def check(self):
        while self.uart.any():
            cmd = self.uart.read(1)
            if cmd == b"B":
                player_id = int(self.uart.read(1).decode())
                state = int(self.uart.read(1).decode())
                if player_id < len(self.players):
                    self.players[player_id].chupi_press = state == 1
            if cmd == b"I":
                player_id = int(self.uart.read(1).decode())
                self.uart.write(f"L{player_id}{ Music.PLAYER_1 + player_id}".encode())
                self.uart.flush()

    def rumble(self, player: Player, rhythm, on=True):
        self.tims[player.id].deinit()
        duration = None
        if rhythm:
            duration = rhythm.pop()
        else:
            on = False
        self.uart.write(f"R{player.id}{1 if on else 0}".encode())
        self.uart.flush()
        if duration:
            self.tims[player.id].init(
                period=duration, mode=Timer.ONE_SHOT, callback=lambda t: self.rumble(player, rhythm, not on)
            )

    def music(self, player, music):
        self.uart.write(f"L{player.id}{music}".encode())
        self.uart.flush()
