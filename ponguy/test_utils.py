import time

from ucontextlib import contextmanager  # pylint: disable=import-error

_times = {}


@contextmanager
def timeit(msg, avg=1):
    start = time.ticks_us()
    yield
    diff = time.ticks_diff(time.ticks_us(), start)

    data = _times.setdefault(msg, {"count": 0, "diff": diff, "fastest": diff, "slowest": diff})
    data["count"] += 1
    mean = data["diff"] = (diff + data["diff"]) / 2
    fastest = data["fastest"] = min(diff, data["fastest"])
    slowest = data["slowest"] = max(diff, data["slowest"])

    if data["count"] < avg:
        return

    log_msg = "%s took %s ms (%s ns)" % (msg, mean // 1000, mean)
    if avg > 1:
        log_msg += ", slowest %s ms (%s ns), fastest %s ms (%s ns)" % (
            slowest // 1000,
            slowest,
            fastest // 1000,
            fastest,
        )
    print(log_msg)

    data["count"] = 0
    data["fastest"] = data["slowest"] = data["diff"] = diff
