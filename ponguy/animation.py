import time
from random import randint, randrange

from ponguy.config import config
from ponguy.constants import RAINBOW_COLORS
from ponguy.player import Player
from ponguy.utils import FakePin, get_random_color


class BaseAnimation:
    duration_ms = 0  # 0 means infinite
    is_done = False
    turn_all_off_when_done = False

    def __init__(self, strip):
        self.strip = strip

    def init_anim(self):
        self.start_time = time.ticks_ms()
        self.previous_time = 0

    def done(self):
        if (
            self.duration_ms
            and time.ticks_diff(time.ticks_ms(), self.start_time) >= self.duration_ms
            or self.is_done
        ):
            if self.turn_all_off_when_done:
                self.strip.turn_all_off()
            return True
        return False

    def check_time(self, ms_delay):
        current_time = time.ticks_ms()
        if time.ticks_diff(current_time, self.previous_time) < ms_delay:
            return False
        self.previous_time = current_time
        return True


class RandomColors(BaseAnimation):
    probability = config.rainbow_bonus_probability

    def __init__(self, strip):
        super().__init__(strip)
        self.duration_ms = 60000

    def play(self):
        i = randint(0, self.strip.num_leds - 1)
        self.strip.turn_on(get_random_color(), i)

    @classmethod
    def can_be_added(cls, screensaver):
        return True


class AutoGameAnimation(BaseAnimation):
    probability = config.auto_game_animation_probability

    def __init__(self, strip):
        from ponguy.game import Game

        super().__init__(strip)
        self.duration_ms = 30000

        p1_color_idx = randrange(0, len(RAINBOW_COLORS))
        p2_color_idx = int(p1_color_idx + len(RAINBOW_COLORS) / 2) % len(RAINBOW_COLORS)
        p1 = Player(0, FakePin(), RAINBOW_COLORS[p1_color_idx], 0, 1, is_a_bot=True)
        p2 = Player(1, FakePin(), RAINBOW_COLORS[p2_color_idx], strip.num_leds - 1, -1, is_a_bot=True)

        self.game = Game(strip, [p1, p2], chupi=False)
        self.game.set_sender(randint(0, 1))
        self.game.init_game(set_animation=False)
        self.game.start_pingpong()

    def play(self):
        self.game.play()

    @classmethod
    def can_be_added(cls, screensaver):
        is_only_animation = bool(screensaver.animation_classes == [cls])
        is_previous_animation = isinstance(screensaver.strip.previous_animation, cls)
        follow_win_animation = isinstance(screensaver.strip.previous_animation, WinAnimation)
        return is_only_animation or not (is_previous_animation or follow_win_animation)


class BaseGameAnimation(BaseAnimation):
    turn_all_off_when_done = True

    def __init__(self, strip, game):
        super().__init__(strip)
        self.game = game

    def build_score_bar(self):
        self.score_bar = []
        for i in range(3):
            self.score_bar.append((0, 0, 0))

        score_length = max(self.game.players[0].initial_life_points, self.game.players[0].life_points)
        for i in range(score_length):
            if i < self.game.players[0].life_points:
                self.score_bar.append(self.game.players[0].color)
            else:
                self.score_bar.append((0, 0, 0))

        score_length = max(self.game.players[1].initial_life_points, self.game.players[1].life_points)
        for i in range(score_length):
            if i >= (score_length - self.game.players[1].life_points):
                self.score_bar.append(self.game.players[1].color)
            else:
                self.score_bar.append((0, 0, 0))

        for i in range(1, 4):
            self.score_bar.append((0, 0, 0))

    def display_score_bar(self):
        middle = self.strip.num_leds // 2
        start = middle - ((len(self.score_bar) + 1) // 2)
        for i in range(start, start + len(self.score_bar)):
            self.strip.turn_on(self.score_bar[i - start], i)


class WinAnimation(BaseGameAnimation):
    spacing = 4

    def __init__(self, strip, game, player):
        super().__init__(strip, game)
        self.player = player
        self.duration_ms = 5000
        self.start = 0
        self.build_score_bar()

    def play(self):
        if not self.check_time(80):
            return

        for i in range(self.start, self.strip.num_leds - 1, self.spacing):
            self.strip.turn_off(i)

        self.start = (self.start - self.player.direction + self.spacing) % self.spacing

        for i in range(self.start, self.strip.num_leds - 1, self.spacing):
            self.strip.turn_on(self.player.color, i)

        self.display_score_bar()


class RoundAnimation(BaseGameAnimation):
    def __init__(self, strip, game, player, first_round=False):
        super().__init__(strip, game)
        self.game = game
        self.player = player
        self.duration_ms = 2500 if not first_round else 1500
        self.show = False
        self.build_score_bar()

    def play(self):
        start = 0
        end = self.strip.num_leds - 1
        middle = self.strip.num_leds // 2
        if self.player.direction > 0:
            end = middle + 1
        else:
            start = middle
        if not self.check_time(400 / (max(self.player.initial_life_points - self.player.life_points, 1))):
            return
        if self.show:
            self.strip.turn_all_off()
        else:
            self.strip.turn_range_on(self.player.color, start, end)
        self.show = not self.show
        self.display_score_bar()
