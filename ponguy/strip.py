import typing

import machine
import micropython
import neopixel

from ponguy.config import config
from ponguy.constants import RAINBOW_COLORS

if typing.TYPE_CHECKING:
    ptr32 = list[int]
    ptr8 = list[int]


class StripHW:
    pass


class NeoPixel(StripHW):
    WORD_SIZE = 3

    def __init__(self, buffer_length):
        self.np = neopixel.NeoPixel(machine.Pin(config.led_pin), buffer_length)
        self.buf = memoryview(self.np.buf)[config.start_offset * 3 : (buffer_length - config.end_offset) * 3]
        self.empty_buf = self.np.buf[config.start_offset * 3 : (buffer_length - config.end_offset) * 3]

    def write(self):
        self.np.write()

    @micropython.native
    def turn_all_off(self):
        self.buf[:] = self.empty_buf

    @micropython.native
    def write_buf(self, buf: bytearray, index: int, red: int, green: int, blue: int):
        buf[3 * index : 3 * (index + 1)] = bytearray((green, red, blue))

    @micropython.native
    def write_buf_val(self, buf: bytearray, index: int, val: bytes):
        buf[3 * index : 3 * (index + 1)] = val

    @micropython.native
    def read_buf_val(self, buf: bytearray, index: int):
        return buf[3 * index : 3 * (index + 1)]

    @micropython.viper
    def fade_range_by(self, start: int, end: int, amount: int):
        buf = ptr8(self.buf)

        for i in range(start, end + 1):
            index = i * 3
            buf[index + 0] = (buf[index + 0] * amount) >> 8
            buf[index + 1] = (buf[index + 1] * amount) >> 8
            buf[index + 2] = (buf[index + 2] * amount) >> 8

    @micropython.native
    def reverse_buffer(self):
        buf = self.buf
        buf_size = int(len(self.buf))
        for i in range(buf_size // 3 // 2):
            fw, rw = 3 * i, buf_size - 3 * (i + 1)
            buf[fw : fw + 3], buf[rw : rw + 3] = buf[rw : rw + 3], buf[fw : fw + 3]

    def get_pixel_color(self, i):
        """Used in unit tests"""
        assert i >= 0
        return tuple(self.buf[3 * i : 3 * (i + 1)])


class APA102C(StripHW):
    START_HEADER_SIZE = 4
    LED_START = 0b11100000  # Three "1" bits, followed by 5 brightness bits
    WORD_SIZE = 4

    def __init__(self, buffer_length):
        self.buffer_length = buffer_length
        self._spi = machine.SPI(
            2,
            baudrate=10000000,
            sck=machine.Pin(config.led_clk_pin),
            mosi=machine.Pin(config.led_pin),
            miso=machine.Pin(19),
        )  # miso pin is not used but required

        brightness = max(min(config.brightness, 30), 1)
        self.brightness_byte = brightness | self.LED_START

        self.allocate_buffer()

    def allocate_buffer(self):
        # taken from https://github.com/mattytrentini/micropython-dotstar

        # Supply one extra clock cycle for each two pixels in the strip.
        self.end_header_size = self.buffer_length // 16
        if self.buffer_length % 16 != 0:
            self.end_header_size += 1
        self._buf = bytearray(self.buffer_length * 4 + self.START_HEADER_SIZE + self.end_header_size)
        self.end_header_index = len(self._buf) - self.end_header_size

        # Four empty bytes to start.
        for i in range(self.START_HEADER_SIZE):
            self._buf[i] = 0x00

        # Set brightness of each pixel.
        for i in range(self.START_HEADER_SIZE, self.end_header_index, 4):
            self._buf[i] = self.brightness_byte

        # null bytes at the end.
        for i in range(self.end_header_index, len(self._buf)):
            self._buf[i] = 0x00

        offset = config.start_offset * 4 + self.START_HEADER_SIZE
        self.buf = memoryview(self._buf)[offset : self.end_header_index]
        self.buf_size = int(len(self.buf) // 4)  # needed for viper in reverse

        self._black_buffer = self._buf[offset : self.end_header_index]

    def write(self):
        self._spi.write(self._buf)

    @micropython.native
    def turn_all_off(self):
        self.buf[:] = self._black_buffer

    @micropython.viper
    def write_buf(self, buf: ptr32, index: int, red: int, green: int, blue: int):
        pbuf = ptr32(buf)
        pbuf[index] = ((red << 8 | green) << 8 | blue) << 8 | int(self.brightness_byte)

    @micropython.viper
    def write_buf_val(self, buf: ptr32, index: int, val: int):
        pbuf = ptr32(buf)
        pbuf[index] = val

    @micropython.viper
    def read_buf_val(self, buf: ptr32, index: int) -> int:
        pbuf = ptr32(buf)
        return int(pbuf[index])

    @micropython.viper
    def fade_range_by(self, start: int, end: int, amount: int):
        buf = ptr8(self.buf)

        for i in range(start, end + 1):
            index = i * 4
            buf[index + 1] = (buf[index + 1] * amount) >> 8
            buf[index + 2] = (buf[index + 2] * amount) >> 8
            buf[index + 3] = (buf[index + 3] * amount) >> 8

    @micropython.viper
    def reverse_buffer(self):
        buf = ptr32(self.buf)
        buf_size = int(self.buf_size)
        for i in range(buf_size // 2):
            buf[i], buf[buf_size - 1 - i] = buf[buf_size - 1 - i], buf[i]

    def get_pixel_color(self, i):
        """Used in unit tests"""
        assert i >= 0
        i += config.start_offset
        index = i * 4 + self.START_HEADER_SIZE
        return tuple(reversed(self._buf[index + 1 : index + 4]))


AVAILABLES_HW = {"APA102C": APA102C, "NeoPixel": NeoPixel}


class Strip:
    animation = None
    previous_animation = None
    reversed_display = False

    def __init__(self):
        self.hw = AVAILABLES_HW[config.strip_hw](config.num_leds + config.start_offset + config.end_offset)
        self.num_leds = config.num_leds

        self.rainbow_buffer = self.get_color_buffer(RAINBOW_COLORS)
        self.rainbow_colors_count = len(RAINBOW_COLORS)

    def show(self):
        if self.reversed_display:
            self.hw.reverse_buffer()
        self.hw.write()

    @micropython.native
    def turn_on(self, color, position):
        self.hw.write_buf(self.hw.buf, position, *color)

    @micropython.native
    def turn_off(self, position):
        self.hw.write_buf(self.hw.buf, position, 0, 0, 0)

    @micropython.native
    def turn_range_on(self, color, start, end):
        self.turn_range_on_viper(color[0], color[1], color[2], start, end)

    @micropython.viper
    def turn_range_on_viper(self, red: int, green: int, blue: int, start: int, end: int):
        for i in range(start, end + 1):
            self.hw.write_buf(self.hw.buf, i, red, green, blue)

    @micropython.viper
    def get_buffer(self, size: int):
        return bytearray(size * int(self.hw.WORD_SIZE))

    @micropython.viper
    def get_color_buffer(self, colors):
        colors_len = int(len(colors))
        buffer = self.get_buffer(colors_len)
        self.fill_color_buffer(colors, buffer, colors_len)
        return buffer

    @micropython.viper
    def fill_color_buffer(self, colors, buffer, buffer_size):
        for i in range(int(buffer_size)):
            self.hw.write_buf(buffer, i, *colors[i])

    @micropython.native
    def turn_range_on_from_buffer(self, colors: bytearray, start: int, end: int, start_offset: int):
        colors_end = start_offset + min(end - start, len(colors) - start_offset) + 1
        self.hw.buf[self.hw.WORD_SIZE * start : self.hw.WORD_SIZE * (end + 1)] = colors[
            self.hw.WORD_SIZE * start_offset : self.hw.WORD_SIZE * colors_end
        ]

    @micropython.viper
    def turn_range_on_rainbow(self, start: int, end: int, rainbow_start: int, step: int):
        colors_count = int(self.rainbow_colors_count)

        for i in range(start, end + 1):
            self.hw.write_buf_val(
                self.hw.buf,
                i,
                self.hw.read_buf_val(self.rainbow_buffer, (rainbow_start + i * step) % colors_count),
            )

    def set_animation(self, animation):
        self.animation = animation
        self.animation.init_anim()

    def animate(self):
        if self.animation:
            if self.animation.done():
                self.previous_animation = self.animation
                self.animation = None
            else:
                self.animation.play()
        return bool(self.animation is not None)

    @micropython.viper
    def fade_range_by(self, start: int, end: int, amount: int):
        self.hw.fade_range_by(start, end, amount)

    @micropython.native
    def turn_all_off(self):
        self.hw.turn_all_off()
