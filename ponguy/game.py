import gc
import time
from random import randint

from ponguy import Colors, bonus
from ponguy.animation import RoundAnimation, WinAnimation
from ponguy.ball import Ball
from ponguy.bonus import Bonus
from ponguy.chupi import Chupi, Music
from ponguy.config import config
from ponguy.player import Player
from ponguy.utils import fade_to_black


class Game:
    WAITING = 0
    GETTING_READY = 1
    PLAYING = 2
    ENDING = 3

    speed = config.delay
    first_ball_index = 0
    state = WAITING
    autoplay_timer = 0
    bonus_classes = [
        bonus.SpeedingBonus,
        bonus.BlinkingBonus,
        bonus.BreakoutBonus,
        bonus.RainbowBonus,
        bonus.BallAddBonus,
        bonus.LifeAddBonus,
        bonus.ReverseBonus,
        bonus.VolleyBonus,
    ]

    def __init__(self, strip, players: list[Player] | None = None, chupi=True):
        self.first_ball_idx = 0
        self.strip = strip
        self.bonuses = []
        if players is None:
            self.players = [
                Player(
                    0,
                    config.red_button_pin,
                    Colors.ORANGE,
                    0,
                    1,
                ),
                Player(
                    1,
                    config.blue_button_pin,
                    Colors.BLUE,
                    strip.num_leds - 1,
                    -1,
                ),
            ]
        else:
            self.players = players
        self.players[0].opponent, self.players[1].opponent = self.players[1], self.players[0]
        self.chupi = Chupi(self.players) if config.chupi_enable and chupi else None

    def init_game(self, new_round=True, set_animation=True):
        self.autoplay_timer = 0
        self.balls = []
        for player in self.players:
            player.init_game()
            if self.chupi:
                self.chupi.music(player, Music.START)
                self.chupi.rumble(player, [200, 200, 200, 200, 200])
        self.start_new_round(self.sender, True, set_animation)

    def should_start(self):
        if self.chupi:
            self.chupi.check()
        if not self.check_buttons():
            # User released the button before starting autoplay mode
            return self.autoplay_timer != 0

        if not self.autoplay_timer:
            self.autoplay_timer = time.ticks_ms()
        elif time.ticks_diff(time.ticks_ms(), self.autoplay_timer) > 4000:
            self.receiver.is_a_bot = True
            return True

        return False

    def check_buttons(self):
        for i in range(2):
            if self.players[i].holding():
                self.set_sender(i)
                return True
        return False

    def set_sender(self, index):
        self.sender = self.players[index]
        self.receiver = self.players[(index + 1) % 2]

    def play(self):
        if self.state == self.GETTING_READY:
            if not self.strip.animate():
                self.start_pingpong()
        elif self.state == self.PLAYING:
            self.pingpong()
        elif self.state == self.ENDING:
            if not self.strip.animate():
                self.state = self.WAITING

    def pingpong(self):
        self.current_micros = time.ticks_us()

        self.strip.turn_all_off()

        if self.chupi:
            self.chupi.check()

        for player in self.players:
            player.check_button_press()

        self.display_hitting_zone_dot()

        self.first_ball_idx = (self.first_ball_idx + 1) % len(self.balls)
        for ball in self.balls[self.first_ball_idx :] + self.balls[: self.first_ball_idx]:
            ball.display()

            if ball.sender.pressing:
                self.check_for_bonus(ball)

            if ball.receiver.send_back(ball):
                self.send_back(ball)

            hitted_player = ball.move()
            if hitted_player:
                if self.chupi:
                    self.chupi.music(hitted_player, Music.BALL_LOST)
                    self.chupi.rumble(hitted_player, [1000])
                self.remove_ball(ball)
                hitted_player.life_points -= 1
                if hitted_player.life_points == 0:
                    return self.end_game(hitted_player.opponent)
                elif len(self.balls) == 0:
                    return self.start_new_round(hitted_player.opponent)

        self.add_scheduled_bonus()
        self.display_bonus()

    def send_back(self, ball):
        if ball.bonus:
            ball.remove_bonus()

        ball.switch_player()

        if ball.sender.bonus:
            ball.sender.bonus.action_on_send_back(ball)

        if ball.speed == ball.first_hit_speed:  # sneaky attack
            ball.set_position(round(self.strip.num_leds / 2))

    def remove_ball(self, ball):
        ball.deactivate()
        self.balls.remove(ball)

    def start_new_round(self, sender, first_round=False, set_animation=True):
        gc.collect()  # force MicroPython garbage collection
        self.bonuses.clear()

        for player in self.players:
            player.bonus = None

        self.sender = sender

        self.strip.reversed_display = False
        self.strip.turn_all_off()

        if set_animation:
            self.strip.set_animation(RoundAnimation(self.strip, self, sender, first_round))
        self.state = self.GETTING_READY

    def start_pingpong(self):
        self.scheduled_bonus_micros = time.ticks_add(time.ticks_us(), self.get_bonus_delay_us())
        self.add_ball(self.sender)
        self.state = self.PLAYING

    def add_ball(self, player):
        self.balls.append(Ball(self, self.strip, player))

    def end_game(self, winner):
        self.players[0].is_a_bot = self.players[1].is_a_bot = False

        self.strip.reversed_display = False
        self.strip.turn_all_off()

        self.strip.set_animation(WinAnimation(self.strip, self, winner))
        self.state = self.ENDING

        if self.chupi:
            self.chupi.music(winner, Music.GAME_WIN)

    def display_hitting_zone_dot(self):
        for player in self.players:
            # Fill a bot hitting zone with its faded color
            if player.is_a_bot:
                color = fade_to_black(player.get_dot_color(), 30)
                if player.direction == 1:
                    start = player.position
                    end = player.first_hit_position
                else:
                    start = player.first_hit_position
                    end = player.position

                self.strip.turn_range_on(color, start, end)

            self.strip.turn_on(player.get_dot_color(), player.first_hit_position)
            self.strip.turn_on(player.get_dot_color(), player.position)

    def display_bonus(self):
        for bonus in self.bonuses:
            bonus.display()

    def get_bonus_delay_us(self):
        min_bonus_delay = 1000000 * int(config.min_bonus_delay)
        max_bonus_delay = 1000000 * int(config.max_bonus_delay)
        return min_bonus_delay + randint(0, max_bonus_delay - min_bonus_delay - 1)

    def add_scheduled_bonus(self):
        if self.current_micros < self.scheduled_bonus_micros:
            return

        if len(self.bonuses) >= config.max_bonus_count:
            return

        total_proba = 0
        for bonus_class in self.bonus_classes:
            if not bonus_class.can_be_added(self):
                continue
            total_proba += bonus_class.get_probability(self)

        if not total_proba:
            return

        proba = randint(0, total_proba - 1)
        incr_proba = 0
        for bonus_class in self.bonus_classes:
            if not bonus_class.can_be_added(self):
                continue
            incr_proba += bonus_class.get_probability(self)
            if proba < incr_proba:
                coord = randint(
                    self.players[0].hitting_range * 3,
                    self.strip.num_leds - bonus_class.length - self.players[1].hitting_range * 3 - 1,
                )
                bonus = bonus_class(self, self.strip, coord)
                self.bonuses.append(bonus)
                break

        self.scheduled_bonus_micros = self.current_micros + self.get_bonus_delay_us()

    def check_for_bonus(self, ball):
        for bonus in self.bonuses:
            if bonus.can_be_taken(ball):
                bonus.action_on_take(ball)
                if self.chupi:
                    self.chupi.music(ball.sender, Music.BONUS)
                    self.chupi.rumble(ball.sender, [200])

        self.bonuses = [bonus for bonus in self.bonuses if bonus.state != Bonus.NONE]
