from random import randint

import machine

from ponguy.config import config


class Player:
    initial_life_points = config.points_to_win
    hitting_range = config.hitting_range
    button_state = 0
    life_points = 0
    is_a_bot = False
    pressing = False
    bonus = None

    def __init__(self, id, button_pin, color, position, direction, is_a_bot=False):
        self.id = id
        self.button_pin = (
            machine.Pin(button_pin, machine.Pin.IN, machine.Pin.PULL_UP) if not config.chupi_enable else None
        )
        self.color = color
        self.position = position
        self.direction = direction
        self.is_a_bot = is_a_bot
        self.first_hit_position = position + direction * (config.hitting_range - 1)
        self.opponent: Player | None = None
        self.chupi_press = False

    def init_game(self):
        self.life_points = self.initial_life_points

    def holding(self):
        if self.button_pin is None:
            return self.chupi_press
        return self.chupi_press or bool(self.button_pin.value() == 0)

    def check_button_press(self):
        self.pressing = self.check_for_pressing()

    def check_for_pressing(self):
        if self.chupi_press:
            return True
        if self.button_pin is None:
            return False
        value = self.button_pin.value()

        if value == 0:
            self.is_a_bot = False

        if self.is_a_bot:
            return True

        if self.button_state == value:
            return False

        self.button_state = value
        return bool(value == 0)

    def send_back(self, ball):
        if self.bonus and self.bonus.send_back(ball):
            return True

        min_position = self.position if self.direction == 1 else self.first_hit_position
        max_position = self.first_hit_position if self.direction == 1 else self.position
        if not ball.is_in_range(min_position, max_position):
            if (
                self.pressing
                and not self.is_a_bot
                and (not self.bonus or not self.bonus.disable_dead_zone)
                and ball.is_in_range(min_position - self.hitting_range, max_position + self.hitting_range)
            ):
                ball.speed = 0
            return False

        if not self.pressing:
            return False

        if self.is_a_bot:
            ball.set_position(randint(min_position, max_position))

        ball.plain_trail = False
        if ball.position == self.first_hit_position:
            ball.speed = ball.first_hit_speed
        elif ball.position == self.position:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
        elif self.direction == 1 and ball.position < 3:
            ball.speed = ball.late_hit_speed
        elif self.direction == -1 and ball.position > self.position - 3:
            ball.speed = ball.late_hit_speed
        else:
            ball.speed = 1

        return True

    def get_dot_color(self):
        return self.bonus.get_color() if self.bonus else self.color
