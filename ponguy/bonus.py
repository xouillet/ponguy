import time

from ponguy import Colors
from ponguy.config import config
from ponguy.constants import RAINBOW_COLORS


class Bonus:
    NONE = 0
    DISPLAYED = 1
    IN_USE = 2

    disable_dead_zone = False

    # Will be overriden in children classes
    color: tuple | None = None
    length = 0
    probability = 0

    def __init__(self, game, strip, coord):
        self.game = game
        self.strip = strip
        self.coord = coord
        self.state = Bonus.DISPLAYED

    def can_be_taken(self, ball):
        return self.state == Bonus.DISPLAYED and ball.is_in_range(self.coord, self.coord + self.length - 1)

    def action_on_take(self, ball):
        if ball.sender.bonus:
            return
        ball.sender.bonus = self
        self.state = Bonus.IN_USE

    def action_on_send_back(self, ball):
        ball.sender.bonus = None
        ball.bonus = self

    def action_on_ball_move(self, ball):
        pass

    def send_back(self, ball):
        return False

    def get_color(self):
        return self.color

    def display(self):
        if self.state != Bonus.DISPLAYED:
            return
        self.strip.turn_range_on(self.get_color(), self.coord, self.coord + self.length - 1)

    def display_ball(self, ball):
        return False

    def get_speed_boost(self):
        return 1

    @classmethod
    def can_be_added(cls, game):
        return len(game.balls) == 1

    @classmethod
    def get_probability(cls, game):
        return cls.probability


class SpeedingBonus(Bonus):
    probability = config.speeding_bonus_probability
    length = config.speeding_bonus_length
    color = Colors.PURPLE

    def action_on_send_back(self, ball):
        super().action_on_send_back(ball)
        self.speed_increase = config.speeding_bonus_initial_boost

    def get_speed_boost(self):
        return self.speed_increase

    def action_on_ball_move(self, ball):
        if ball.bonus == self:
            self.speed_increase += (
                config.speeding_bonus_final_boost - config.speeding_bonus_initial_boost
            ) / (self.strip.num_leds - 2 * config.hitting_range)


class BlinkingBonus(Bonus):
    probability = config.blinking_bonus_probability
    length = config.blinking_bonus_length
    color = Colors.YELLOW

    blinking_delay = 150

    def __init__(self, *args):
        super().__init__(*args)
        self.show = True
        self.previous_time = 0

    def action_on_take(self, ball):
        if ball.bonus:
            return
        ball.bonus = self
        self.state = Bonus.IN_USE

    def check_time(self):
        current_time = time.ticks_ms()
        # blink is not even, the ball is shown a bit more than it is hidden
        actual_delay = self.blinking_delay * 1.5 if self.show else self.blinking_delay
        if time.ticks_diff(current_time, self.previous_time) < actual_delay:
            return False
        self.previous_time = current_time
        return True

    def get_color(self):
        if self.check_time():
            self.show = not self.show

        if self.show:
            return super().get_color()
        else:
            return Colors.BLACK

    @classmethod
    def get_probability(cls, game):
        factor = 1
        if any(isinstance(bonus, BreakoutBonus) for bonus in game.bonuses):
            factor = 2
        return factor * cls.probability


class BreakoutBonus(Bonus):
    probability = config.breakout_bonus_probability
    length = config.breakout_bonus_length
    color = Colors.LIGHT_WHITE

    blinking_delay = 120000

    def __init__(self, *args):
        super().__init__(*args)
        self.show = True
        self.previous_micros = self.game.current_micros

    def check_time(self):
        if time.ticks_diff(self.game.current_micros, self.previous_micros) < self.blinking_delay:
            return False
        self.previous_micros = self.game.current_micros
        return True

    def get_color(self):
        if self.state == Bonus.IN_USE:
            return super().get_color()

        if self.check_time():
            self.show = not self.show

        if self.show:
            return super().get_color()
        else:
            return Colors.BLACK

    def display(self):
        if self.state == Bonus.NONE:
            return
        self.strip.turn_range_on(self.get_color(), self.coord, self.coord + self.length)

    def action_on_take(self, ball):
        self.state = Bonus.IN_USE

    def action_on_ball_move(self, ball):
        if self.state != Bonus.IN_USE:
            return
        if isinstance(ball.bonus, BlinkingBonus):
            return
        if ball.sender.direction == 1 and ball.position != self.coord - 1:
            return
        if ball.sender.direction == -1 and ball.position != self.coord + self.length + 1:
            return
        self.state = Bonus.NONE
        ball.switch_player()


class LifeAddBonus(Bonus):
    probability = config.life_add_bonus_probability
    length = config.life_add_bonus_length
    color = Colors.GREEN

    def action_on_take(self, ball):
        if ball.sender.life_points >= 2 * ball.sender.initial_life_points:
            self.state = Bonus.NONE
            return

        ball.sender.life_points += 1
        if ball.bonus:
            self.state = Bonus.NONE
        else:
            ball.bonus = self
            self.state = Bonus.IN_USE

    @classmethod
    def can_be_added(cls, game):
        return True


class BallAddBonus(Bonus):
    probability = config.ball_add_bonus_probability
    length = config.ball_add_bonus_length
    go_towards_blue = True
    red = 255
    blue = 0

    def action_on_take(self, ball):
        self.game.add_ball(ball.sender)
        self.state = Bonus.NONE

    @staticmethod
    def get_color_transition_increment(pixel):
        if pixel <= 128:
            return pixel / 128 + 0.4
        else:
            return -1 * pixel / 255 + 1.4

    def get_color(self):
        increment = self.get_color_transition_increment(self.red)

        if self.go_towards_blue:
            self.red -= increment
            self.blue += increment
        else:
            self.red += increment
            self.blue -= increment

        if self.red <= 0 or self.blue >= 255:
            self.go_towards_blue = False
            self.red = 0
            self.blue = 255
        elif self.red >= 255 or self.blue <= 0:
            self.go_towards_blue = True
            self.red = 255
            self.blue = 0

        return (int(self.red), 0, int(self.blue))

    @classmethod
    def can_be_added(cls, game):
        return len(game.balls) < 3


class RainbowBonus(Bonus):
    probability = config.rainbow_bonus_probability
    length = config.rainbow_bonus_length
    color_index = 0

    disable_dead_zone = True

    def __init__(self, *args):
        super().__init__(*args)
        self.previous_micros = self.game.current_micros

    def action_on_send_back(self, ball):
        super().action_on_send_back(ball)
        try:
            self.color_index = RAINBOW_COLORS.index(ball.sender.color)
        except ValueError:
            self.color_index = 0

    def send_back(self, ball):
        if ball.position == ball.receiver.first_hit_position:
            ball.speed = ball.perfect_hit_speed
            ball.plain_trail = True
            return True
        return False

    def get_color(self):
        while self.previous_micros + 2000 < self.game.current_micros:
            self.color_index += 1
            self.previous_micros += 2000

        return RAINBOW_COLORS[self.color_index % len(RAINBOW_COLORS)]

    def display_ball(self, ball):
        if ball.sender.direction == 1:
            start = ball.start_position
            end = ball.position
            color_index = self.color_index - start * 7
        else:
            start = ball.position
            end = ball.start_position
            color_index = self.color_index
            color_index = self.color_index - end * 7

        self.strip.turn_range_on_rainbow(start, end, color_index, 7)

        if ball.speed == 0:
            self.strip.fade_range_by(start, end, ball.get_fading_factor())
        return True


class ReverseBonus(Bonus):
    probability = config.reverse_bonus_probability
    length = config.reverse_bonus_length

    blinking_delay = 300000

    def __init__(self, *args):
        super().__init__(*args)
        self.previous_micros = self.game.current_micros
        self.first_color = self.game.players[0].color
        self.second_color = self.game.players[1].color

    def action_on_take(self, ball):
        self.strip.reversed_display = not self.strip.reversed_display
        self.state = Bonus.NONE

    def display(self):
        if self.state == Bonus.NONE:
            return

        if self.check_time():
            self.first_color, self.second_color = self.second_color, self.first_color

        self.strip.turn_range_on(self.first_color, self.coord, self.coord + self.length // 2 - 1)
        self.strip.turn_range_on(
            self.second_color, self.coord + self.length // 2, self.coord + self.length - 1
        )

    def check_time(self):
        if time.ticks_diff(self.game.current_micros, self.previous_micros) < self.blinking_delay:
            return False
        self.previous_micros = self.game.current_micros
        return True


class VolleyBonus(Bonus):
    probability = config.volley_bonus_probability
    length = config.volley_bonus_length

    color = Colors.ORANGE
    player = None

    def action_on_take(self, ball):
        if ball.sender.bonus:
            return
        ball.sender.bonus = self
        self.state = Bonus.IN_USE
        self.player = ball.sender
        self.color = ball.sender.color

    def send_back(self, ball):
        if (
            self.player
            and ball.receiver == self.player
            and self.player.pressing
            and ball.is_in_range(self.coord, self.coord + self.length - 1)
        ):
            return True
        return False

    def action_on_send_back(self, ball):
        pass

    def action_on_ball_move(self, ball):
        if self.state != Bonus.IN_USE:
            return
        if self.player is None:
            return
        if ball.receiver != self.player:
            return
        if self.player.direction > 0 and ball.position < self.coord:
            self.state = Bonus.NONE
            self.player.bonus = None
        if self.player.direction < 0 and ball.position > self.coord + self.length - 1:
            self.state = Bonus.NONE
            self.player.bonus = None

    def display(self):
        if self.state == Bonus.DISPLAYED:
            self.strip.turn_range_on(self.get_color(), self.coord, self.coord + self.length - 1)
            self.strip.fade_range_by(self.coord + 1, self.coord + self.length - 2, 30)
        elif self.state == Bonus.IN_USE:
            self.strip.turn_on(self.get_color(), self.coord)
            self.strip.turn_on(self.get_color(), self.coord + self.length - 1)

    @classmethod
    def can_be_added(cls, game):
        return (
            Bonus.can_be_added(game) and len([bonus for bonus in game.bonuses if isinstance(bonus, cls)]) < 2
        )
