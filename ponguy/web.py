import asyncio
import json

import machine
import network
import tinyweb

from ponguy.config import OVERRIDE_JSON_PATH, config

app = tinyweb.webserver()


async def delay_reboot():
    await asyncio.sleep(1)
    machine.reset()


class ConfigEndpoint:
    def get(self, data):
        out = {}
        for key in dir(config):
            if key.startswith("_"):
                continue
            try:
                meta = getattr(config, f"_{key}_meta")
            except AttributeError:
                continue

            out[key] = {
                "value": getattr(config, key),
                "default": getattr(config.__class__, key),
                "meta": meta,
            }
        return out

    def post(self, data):
        print(f"Writing to {OVERRIDE_JSON_PATH}...")
        print(data)
        with open(OVERRIDE_JSON_PATH, "w") as file:
            json.dump(data, file)
        asyncio.create_task(delay_reboot())
        return {"applied": True}, 201


class StateEndpoint:
    def get(self, data, state):
        return state


async def index(req, response):
    await response.send_file("/webui/index.html")


async def assets(req, response, path):
    ct = "text/html"
    if path.endswith(".css"):
        ct = "text/css"
    elif path.endswith(".js"):
        ct = "application/javascript"
    await response.send_file(f"/webui/assets/{path}", content_type=ct)


class WebBackend:
    def __init__(self, state):
        wlan_id = "Ponguy"
        wlan_pass = "pipopipo"

        wlan = network.WLAN(network.AP_IF)
        wlan.active(True)
        wlan.config(ssid=wlan_id, key=wlan_pass)
        wlan.ifconfig(("192.168.8.1", "255.255.255.0", "192.168.8.1", "192.168.8.1"))

        self.state = state

    def run(self):
        app.add_route("/", index)
        app.add_route("/index.html", index)
        app.add_route("/assets/<path>", assets)
        app.add_resource(ConfigEndpoint, "/config")
        app.add_resource(StateEndpoint, "/state", state=self.state)
        app.run(host="0.0.0.0", port=80, loop_forever=False)
