import asyncio

import machine

from ponguy.config import config
from ponguy.game import Game
from ponguy.screensaver import Screensaver
from ponguy.strip import Strip
from ponguy.web import WebBackend


async def ponguy(state):
    try:
        strip = Strip()
        screensaver = Screensaver(strip)

        game = Game(strip)

        for pin in config.pins_to_power:
            pin = machine.Pin(pin, machine.Pin.OUT)
            pin.on()
        while True:
            strip.show()
            if game.state != game.WAITING:
                game.play()
            elif game.should_start():
                game.init_game()
            else:
                screensaver.animate()
            await asyncio.sleep(0)
    except Exception as e:
        state["exc"] = e
        print(e)


def run():
    state = {}

    # Web task
    back = WebBackend(state)
    back.run()

    # Ponguy task
    loop = asyncio.get_event_loop()
    loop.create_task(ponguy(state))

    # Main loop
    loop.run_forever()
