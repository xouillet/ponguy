import time

import micropython

from ponguy.bonus import Bonus
from ponguy.config import config

from .utils import fade_to_black


class Ball:
    length = config.ball_length
    perfect_hit_speed = config.perfect_hit_boost
    late_hit_speed = config.late_hit_boost
    first_hit_speed = config.first_hit_boost

    def __init__(self, game, strip, player):
        self.game = game
        self.strip = strip
        self.speed = 1
        self.plain_trail = False
        self.previous_micros = time.ticks_us()
        self.sender = player
        self.receiver = player.opponent
        self.start_position = self.sender.position
        self.position = self.start_position
        self.bonus = None
        self.previous_color = None
        self.colors_buffer = self.strip.get_buffer(self.length)

    @micropython.native
    def is_in_range(self, start, end):
        return bool(self.position >= start and self.position <= end)

    @micropython.native
    def get_fading_factor(self):
        diff = time.ticks_diff(self.game.current_micros, self.previous_micros)
        fading_delay = config.fading_delay_multiball if len(self.game.balls) > 1 else config.fading_delay
        return max(
            0,
            int(256 * (1 - (diff / (fading_delay * 1000000)))),
        )

    @micropython.native
    def display(self):
        if self.bonus and self.bonus.display_ball(self):
            return

        color = self.bonus.get_color() if self.bonus else self.sender.color

        if self.plain_trail:
            if self.sender.direction == 1:
                start = self.start_position
                end = self.position
            else:
                start = self.position
                end = self.start_position
            self.strip.turn_range_on(color, start, end)
        else:
            if self.sender.direction == 1:
                end = self.position
                start = max(self.start_position, end - self.length + 1)
                start_offset = self.length - 1 - (end - start)
            else:
                start = self.position
                end = min(self.start_position, start + self.length - 1)
                start_offset = 0

            if color != self.previous_color:
                colors = []
                for i in range(1, self.length + 1):
                    colors.append(fade_to_black(color, i / self.length * 256))

                if self.sender.direction == -1:
                    colors.reverse()

                self.strip.fill_color_buffer(colors, self.colors_buffer, self.length)
                self.previous_color = color

            self.strip.turn_range_on_from_buffer(self.colors_buffer, start, end, start_offset)

        if self.speed == 0:
            self.strip.fade_range_by(start, end, self.get_fading_factor())

    def deactivate(self):
        if self.bonus:
            self.remove_bonus()

    def remove_bonus(self):
        if self.bonus:
            self.bonus.state = Bonus.NONE
        self.bonus = None

    def set_bonus(self, bonus: Bonus):
        self.bonus = bonus

    def switch_player(self):
        self.sender, self.receiver = self.receiver, self.sender
        self.start_position = self.position

    def get_speed(self):
        bonus_speed_boost = self.bonus.get_speed_boost() if self.bonus else 1
        return self.speed * bonus_speed_boost

    @micropython.native
    def move(self):
        speed = self.get_speed()
        if speed == 0:
            fading_delay = config.fading_delay_multiball if len(self.game.balls) > 1 else config.fading_delay
            if time.ticks_diff(self.game.current_micros, self.previous_micros) >= fading_delay * 1000000:
                return self.receiver
            return None

        speed = self.game.speed / speed
        while time.ticks_diff(self.game.current_micros, self.previous_micros) >= speed:
            self.previous_micros = time.ticks_add(self.previous_micros, int(speed))
            self.position += self.sender.direction
            if not self.is_in_range(0, self.strip.num_leds - 1):
                return self.receiver
            for bonus in self.game.bonuses:
                bonus.action_on_ball_move(self)

        return None

    def set_position(self, position):
        self.start_position = position
        self.position = position
