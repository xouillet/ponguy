import json
from typing import Literal

OVERRIDE_JSON_PATH: str = "./config.json"


class PonguyConfig:
    # Led strip configuration
    strip_hw: Literal["APA102C"] | Literal["NeoPixel"] = "NeoPixel"
    _strip_hw_meta = {
        "category": "LED strip",
        "type": "enum",
        "enum": ["APA102C", "NeoPixel"],
        "desc": "LED strip hardware type",
    }
    led_pin = 23
    _led_pin_meta = {"category": "LED strip", "type": "pin", "desc": "GPIO pin for led data"}
    led_clk_pin = 18
    _led_clk_pin_meta = {
        "category": "LED strip",
        "type": "pin",
        "desc": "GPIO pin for led clk (APA102C only)",
    }
    num_leds = 300
    _num_leds_meta = {
        "category": "LED strip",
        "type": "int",
        "desc": "Total number of leds on strip, even not playable one",
    }
    start_offset = 0
    _start_offset_meta = {"category": "LED strip", "type": "int", "desc": "Offset for first led"}
    end_offset = 0
    _end_offset_meta = {"category": "LED strip", "type": "int", "desc": "Offset for last led"}
    brightness = 100
    _brightness_meta = {"category": "LED strip", "type": "percentage", "desc": "Brightness in percentage"}

    # Input configuration
    pins_to_power: list[int] = []
    _pins_to_power_meta = {
        "category": "Input",
        "type": "list",
        "list": "pin",
        "desc": "Set these gpios to high at startup",
    }
    blue_button_pin = 17
    _blue_button_pin_meta = {"category": "Input", "type": "pin", "desc": "Input gpio pin for blue"}
    red_button_pin = 16
    _red_button_pin_meta = {"category": "Input", "type": "pin", "desc": "Input gpio pin for red"}
    chupi_enable = False
    _chupi_enable_meta = {"category": "Input", "type": "bool", "desc": "Enable host usb support"}
    chupi_uart_id = 1
    _chupi_uart_id_meta = {"category": "Input", "type": "int", "desc": "Uart ID for chupi"}
    chupi_uart_tx = 8
    _chupi_uart_tx_meta = {"category": "Input", "type": "pin", "desc": "Uart TX pin for chupi"}
    chupi_uart_rx = 9
    _chupi_uart_rx_meta = {"category": "Input", "type": "pin", "desc": "Uart RX pin for chupi"}

    # Game configuration
    delay = 5000
    _delay_meta = {
        "category": "Game",
        "type": "int",
        "desc": "Delay in ms between 2 ticks. Higher means slower game",
    }
    ball_length = 5
    _ball_length_meta = {"category": "Game", "type": "int", "desc": "Ball size in pixels"}
    points_to_win = 3
    _points_to_win_meta = {
        "category": "Game",
        "type": "int",
        "desc": "Number of lives at game beggining",
    }
    hitting_range = 10
    _hitting_range_meta = {"category": "Game", "type": "int", "desc": "Racket size"}
    perfect_hit_boost = 2.0
    _perfect_hit_boost_meta = {
        "category": "Game",
        "type": "float",
        "desc": "Boost when ball is hit perfectly",
    }
    late_hit_boost = 1.5
    _late_hit_boost_meta = {
        "category": "Game",
        "type": "float",
        "desc": "Boost when ball is ball is hit late",
    }
    first_hit_boost = 1.2
    _first_hit_boost_meta = {"category": "Game", "type": "float", "desc": "Boost when ball is first hit"}

    # Bonuses
    min_bonus_delay: float = 2.0
    _min_bonus_delay_meta = {
        "category": "Bonus",
        "type": "float",
        "desc": "Minimum delay before a bonus appears",
    }
    max_bonus_delay: float = 20.0
    _max_bonus_delay_meta = {
        "category": "Bonus",
        "type": "float",
        "desc": "Maximum delay before a bonus appears",
    }
    max_bonus_count: int = 4
    _max_bonus_count_meta = {"category": "Bonus", "type": "float", "desc": "Maximum bonus present"}
    speeding_bonus_probability: float = 55
    speeding_bonus_length: float = 5
    speeding_bonus_initial_boost: float = 0.8
    speeding_bonus_final_boost: float = 2.0
    rainbow_bonus_probability: float = 40
    rainbow_bonus_length: float = 5
    blinking_bonus_probability: float = 80
    blinking_bonus_length: float = 6
    life_add_bonus_probability: float = 100
    life_add_bonus_length: float = 3
    ball_add_bonus_probability: float = 65
    ball_add_bonus_length: float = 6
    breakout_bonus_probability: float = 80
    breakout_bonus_length: float = 5
    portal_bonus_probability: float = 40
    portal_bonus_length: float = 5
    volley_bonus_probability: float = 65
    volley_bonus_length: float = hitting_range - 2
    reverse_bonus_probability: float = 0
    reverse_bonus_length: float = 6

    # Screensaver
    random_colors_animation_probability: float = 10  # probability of rainbow animation
    _random_colors_animation_probability_meta = {
        "category": "Screensaver",
        "type": "float",
        "desc": "Probability of rainbow animation",
    }
    auto_game_animation_probability: float = 1  # probability of auto game
    _auto_game_animation_probability_meta = {
        "category": "Screensaver",
        "type": "float",
        "desc": "Probability of auto game",
    }

    # Timings
    win_animation_seconds: float = 5  # animation time when game stops
    round_animation_seconds: float = 2  # animation time when game starts
    fading_delay: float = 1  # delay before ball disappear
    fading_delay_multiball: float = 0.33  # delay before ball disappear in case of multiball

    # System
    native_viper: bool = True  # enable or disable viper/native mpython


config = PonguyConfig()

try:
    with open(OVERRIDE_JSON_PATH, "r") as override:
        for k, v in json.load(override).items():
            setattr(config, k, v)
except Exception as e:
    print(e)
