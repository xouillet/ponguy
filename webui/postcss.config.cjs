/* eslint-disable @typescript-eslint/no-var-requires */
const purgecss = require('@fullhuman/postcss-purgecss')

module.exports = {
  plugins: [
    purgecss({
      content: [
        './index.html',
        './src/**/*.{svelte,js,ts}',
      ], // declaring source files
      safelist: {
        standard: [/$svelte-/], // required for inline component styles
      },
      variables: true, // remove unused CSS variables
    }),
  ],
}