import time

from ponguy import Colors
from ponguy.bonus import (
    BallAddBonus,
    BlinkingBonus,
    Bonus,
    BreakoutBonus,
    LifeAddBonus,
    RainbowBonus,
    ReverseBonus,
    SpeedingBonus,
    VolleyBonus,
)

from .utils import (
    get_pixel_color,
    get_range_color,
    make_playing_game,
    mock_button_press,
)


def test_bonus_general_mechanism(freezer):
    game = make_playing_game()
    game.bonus_classes = [LifeAddBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    game.pingpong()
    assert game.bonuses == []

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()

    life_add_bonus = game.bonuses[0]
    assert life_add_bonus.state == LifeAddBonus.DISPLAYED
    assert life_add_bonus.coord > game.balls[0].position

    assert get_pixel_color(game, life_add_bonus.coord - 1) == Colors.BLACK
    assert (
        get_range_color(game, life_add_bonus.coord, life_add_bonus.coord + life_add_bonus.length)
        == life_add_bonus.color
    )
    assert get_pixel_color(game, life_add_bonus.coord + life_add_bonus.length) == Colors.BLACK

    ball = game.balls[0]
    assert not ball.bonus
    assert ball.sender.life_points == 3

    # move ball on bonus
    target_time = game.speed * (life_add_bonus.coord - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent tries to take bonus
    with mock_button_press(ball.receiver):
        game.pingpong()

    # nothing happens
    assert life_add_bonus.state == LifeAddBonus.DISPLAYED

    # sender takes bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert ball.bonus.__class__ == LifeAddBonus
    assert not ball.sender.bonus
    assert ball.sender.life_points == 4
    assert life_add_bonus.state == LifeAddBonus.IN_USE

    game.pingpong()
    assert get_pixel_color(game, ball.position) == life_add_bonus.color


def test_speeding_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [SpeedingBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    speeding_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (speeding_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert speeding_bonus.state == SpeedingBonus.IN_USE
    assert not ball.bonus
    assert ball.sender.bonus.__class__ == SpeedingBonus

    # hitting zone dot color changed
    game.pingpong()
    assert get_pixel_color(game, ball.sender.position) == speeding_bonus.color

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to player hitting zone
    target_time = game.speed * (ball.position - ball.receiver.first_hit_position + 1)
    freezer.tick(target_time)
    game.pingpong()

    # player sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    assert speeding_bonus.state == SpeedingBonus.IN_USE
    assert ball.bonus.__class__ == SpeedingBonus
    assert not ball.sender.bonus

    for _ in range(100):
        freezer.tick(game.speed)
        game.pingpong()

    assert ball.position == 138  # ball has moved faster


def test_breakout_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [BreakoutBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    breakout_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (breakout_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    # TODO check still displayed and does not blink anymore
    assert breakout_bonus.state == BreakoutBonus.IN_USE
    assert not ball.bonus
    assert not ball.sender.bonus

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before breakout bonus
    target_time = game.speed * (ball.position - breakout_bonus.coord - breakout_bonus.length - 2)
    freezer.tick(target_time)
    game.pingpong()
    assert breakout_bonus.state == BreakoutBonus.IN_USE
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    assert breakout_bonus.state == BreakoutBonus.NONE
    assert ball.sender.direction == 1  # ball is going back


def test_breakout_bonus_blinking_bypass(freezer):
    game = make_playing_game()
    game.bonus_classes = []
    ball = game.balls[0]
    game.pingpong()

    breakout_bonus = BreakoutBonus(game, game.strip, 50)
    game.bonuses.append(breakout_bonus)

    blinking_bonus = BlinkingBonus(game, game.strip, 100)
    game.bonuses.append(blinking_bonus)

    # move ball on breakout bonus
    target_time = game.speed * (breakout_bonus.coord - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert breakout_bonus.state == BreakoutBonus.IN_USE

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball on blinking bonus
    target_time = game.speed * (ball.position - blinking_bonus.coord)
    freezer.tick(target_time)
    game.pingpong()

    # activate bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert blinking_bonus.state == BlinkingBonus.IN_USE

    # move ball to just before breakout bonus
    target_time = game.speed * (ball.position - breakout_bonus.coord - breakout_bonus.length - 2)
    freezer.tick(target_time)
    game.pingpong()
    assert breakout_bonus.state == BreakoutBonus.IN_USE
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    assert breakout_bonus.state == BreakoutBonus.IN_USE
    assert ball.sender.direction == -1  # ball is still going in the same direction


def test_ball_add_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [BallAddBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    ball_add_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (ball_add_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert not ball.sender.bonus
    assert not ball.bonus
    assert ball_add_bonus.state == BallAddBonus.NONE

    first_ball_position = game.balls[0].position
    second_ball_position = game.balls[1].position

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # two balls are now moving
    assert game.balls[0].position > first_ball_position
    assert game.balls[1].position > second_ball_position


def test_rainbow_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]
    game.bonus_classes = [RainbowBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    rainbow_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (rainbow_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert rainbow_bonus.state == RainbowBonus.IN_USE
    assert not ball.bonus
    assert ball.sender.bonus.__class__ == RainbowBonus

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before player hitting zone
    target_time = game.speed * (ball.position - ball.receiver.first_hit_position - 1)
    freezer.tick(target_time)
    game.pingpong()

    game.pingpong()
    assert ball.sender.direction == -1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # rainbow starts with player color
    assert get_pixel_color(game, ball.sender.first_hit_position) == (255, 0, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 1) == (237, 18, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 2) == (218, 37, 0)

    assert ball.sender.direction == 1  # ball is going back automatically
    assert ball.bonus.__class__ == RainbowBonus
    assert not ball.sender.bonus

    for _ in range(50):
        freezer.tick(game.speed)
        game.pingpong()

    assert ball.position == 127  # ball is moving fast

    # rainbow is stable
    assert get_pixel_color(game, ball.sender.first_hit_position) == (255, 0, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 1) == (237, 18, 0)
    assert get_pixel_color(game, ball.sender.first_hit_position + 2) == (218, 37, 0)


def test_rainbow_bonus_reverse(freezer):
    game = make_playing_game(opposite_player_sends=True)
    ball = game.balls[0]
    game.bonus_classes = [RainbowBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    rainbow_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (game.balls[0].position - rainbow_bonus.coord)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    # move ball to opponent hitting zone
    target_time = game.speed * abs(ball.receiver.first_hit_position - ball.position - 1)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to just before player hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position - 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    game.pingpong()
    assert ball.sender.direction == 1

    for _ in range(10):
        freezer.tick(game.speed)
        game.pingpong()

    # rainbow starts with player color
    assert get_pixel_color(game, ball.sender.first_hit_position) == (0, 0, 255)
    assert get_pixel_color(game, ball.sender.first_hit_position - 1) == (0, 38, 218)
    assert get_pixel_color(game, ball.sender.first_hit_position - 2) == (0, 75, 181)

    assert ball.sender.direction == -1  # ball is going back automatically

    freezer.tick(game.speed * 2)
    game.pingpong()

    # rainbow is stable
    assert get_pixel_color(game, ball.sender.first_hit_position) == (0, 0, 255)
    assert get_pixel_color(game, ball.sender.first_hit_position - 1) == (0, 38, 218)
    assert get_pixel_color(game, ball.sender.first_hit_position - 2) == (0, 75, 181)


def test_bonus_probabilities(freezer):
    class FirstBonus(LifeAddBonus):
        pass

    class SecondBonus(LifeAddBonus):
        pass

    class ThirdBonus(LifeAddBonus):
        pass

    game = make_playing_game()
    game.bonus_classes = [FirstBonus, SecondBonus, ThirdBonus]

    for bonus in game.bonus_classes:
        bonus.probability = 0

    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    game.pingpong()
    assert game.bonuses == []

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    assert game.bonuses == []

    SecondBonus.probability = 1
    game.pingpong()
    assert len(game.bonuses) == 1
    assert game.bonuses[0].__class__ == SecondBonus


def test_reverse_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]

    ReverseBonus.probability = 1
    game.bonus_classes = [ReverseBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    reverse_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (reverse_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert not ball.sender.bonus
    assert not ball.bonus
    assert reverse_bonus.state == ReverseBonus.NONE
    assert game.strip.reversed_display is True

    game.pingpong()  # cover Strip.reverse_buffer()

    # lose ball
    freezer.tick(game.speed * 150)
    game.pingpong()
    assert game.strip.reversed_display is False


def test_volley_bonus(freezer):
    game = make_playing_game()
    ball = game.balls[0]

    VolleyBonus.probability = 1
    game.bonus_classes = [VolleyBonus]
    game.scheduled_bonus_micros = time.ticks_us() + game.speed * 3

    freezer.move_to(game.scheduled_bonus_micros)
    game.pingpong()
    volley_bonus = game.bonuses[0]

    # move ball on bonus
    target_time = game.speed * (volley_bonus.coord - game.balls[0].position)
    freezer.tick(target_time)
    game.pingpong()

    # take bonus
    with mock_button_press(ball.sender):
        game.pingpong()

    assert ball.sender.bonus
    assert not ball.bonus
    assert volley_bonus.state == Bonus.IN_USE

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back
    with mock_button_press(ball.receiver):
        game.pingpong()

    # move ball to bonus
    target_time = game.speed * (ball.position - volley_bonus.coord - volley_bonus.length / 2)
    freezer.tick(target_time)
    game.pingpong()
    assert volley_bonus.state == VolleyBonus.IN_USE
    assert ball.sender.direction == -1

    # first player sends back with volley bonus
    with mock_button_press(ball.receiver):
        game.pingpong()
    assert ball.sender.direction == 1
    assert volley_bonus.state == VolleyBonus.IN_USE

    # move ball to opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position + 1 - ball.position)
    freezer.tick(target_time)
    game.pingpong()

    # opponent sends back again
    with mock_button_press(ball.receiver):
        game.pingpong()
    assert ball.sender.direction == -1

    # move ball to after bonus
    target_time = game.speed * (ball.position - volley_bonus.coord + 1)
    freezer.tick(target_time)
    game.pingpong()
    assert volley_bonus.state == VolleyBonus.NONE  # bonus is lost
    assert ball.sender.direction == -1
