import builtins


class ByteArray32:
    def __init__(self, array):
        self.array = array

    def __setitem__(self, index, value):
        real_index = index * 4
        for i, j in enumerate(range(real_index, real_index + 4)):
            self.array[j] = (value >> i * 8) & 0xFF

    def __getitem__(self, index):
        value = 0
        real_index = index * 4
        for i in range(real_index + 3, real_index - 1, -1):
            value <<= 8
            value |= self.array[i]
        return value


builtins.ptr8 = lambda x: x  # type: ignore
builtins.ptr32 = ByteArray32  # type: ignore


def const(c):
    return c


def native(f):
    def decorate(*args, **kwargs):
        return f(*args, **kwargs)

    return decorate


def viper(f):
    def decorate(*args, **kwargs):
        return f(*args, **kwargs)

    return decorate
