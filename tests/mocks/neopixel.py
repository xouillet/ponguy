from typing import Any


class NeoPixel:
    ORDER: Any
    pin: Any
    n: Any
    bpp: Any
    buf: Any
    timing: Any

    def __init__(self, pin, n, bpp: int = ..., timing: int = ...) -> None:
        self.buf = bytearray(n * 3)

    def __len__(self) -> int: ...

    def __setitem__(self, i, v) -> None: ...

    def __getitem__(self, i): ...

    def fill(self, v) -> None: ...

    def write(self) -> None: ...
