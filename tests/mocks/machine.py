class Pin:
    OUT = "out"
    IN = "in"
    PULL_UP = "pull_up"

    def __init__(self, no, in_out=None, pull_up=None):
        self.no = no
        self.in_out = in_out

    def value(self, on_off=None):
        return 1

    def on(self):
        pass


class SPI:
    def __init__(self, id_, baudrate, sck, mosi, miso):
        pass

    def write(self, buffer):
        pass


class UART:
    def __init__(self, *args, **kwargs):
        pass


class Timer:
    def __init__(self, *args, **kwargs):
        pass
