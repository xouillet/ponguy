from time import *  # type: ignore # pylint: disable=wildcard-import,unused-wildcard-import


def ticks_ms():
    return time_ns() // 10**6


def ticks_us():
    return time_ns() // 10**3


def ticks_diff(a, b):
    return a - b


def ticks_add(a, b):
    return a + b
