import colorsys
import time
from unittest import mock

from ponguy import Colors
from ponguy.config import config
from ponguy.game import Game
from ponguy.screensaver import Screensaver
from ponguy.strip import Strip

from .utils import (
    get_pixel_color,
    get_range_color,
    make_playing_game,
    mock_button_press,
)

config.strip_hw = "APA102C"  # Tests are written for this HW...
config.num_leds = 150

strip = Strip()
screensaver = Screensaver(strip)

game = Game(strip)


def run():
    strip.show()

    if game.state != game.WAITING:
        game.play()
    elif game.should_start():
        game.init_game()
    else:
        screensaver.animate()


def test_full_game(freezer):
    run()
    assert game.state == game.WAITING

    # red player starts game
    def mock_red_button_press(self, on_off=None):
        if self.no == config.red_button_pin:
            return 0

    with mock.patch("machine.Pin.value", mock_red_button_press):
        run()

    assert game.state == game.WAITING

    run()
    assert game.state == game.GETTING_READY
    assert game.sender.button_pin.no == config.red_button_pin

    # strip is animating
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    assert game.strip.animation is not None
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    assert game.balls[0].position == 0

    # ball won't move until some time
    run()
    assert game.balls[0].position == 0

    # hitting zones are displayed
    assert get_pixel_color(game, 0) == game.sender.color
    assert get_pixel_color(game, config.hitting_range - 1) == game.sender.color
    assert get_range_color(game, 1, config.hitting_range - 1) == Colors.BLACK

    assert get_pixel_color(game, game.strip.num_leds - 1) == game.receiver.color
    assert get_pixel_color(game, game.strip.num_leds - config.hitting_range) == game.receiver.color
    assert get_range_color(game, game.strip.num_leds - config.hitting_range + 1, game.strip.num_leds - 1) == (
        0,
        0,
        0,
    )

    # accelerate time until ball moves
    freezer.tick(game.speed)
    run()
    assert game.balls[0].position == 1

    run()
    assert get_pixel_color(game, 1) == game.sender.color
    assert get_pixel_color(game, 2) == Colors.BLACK

    for i in range(2, 145):
        freezer.tick(game.speed)
        run()
        assert game.balls[0].position == i

        run()
        ball_last_pixel_index = max(0, i - config.ball_length + 1)

        # check ball end, ignoring hitting zone dots
        if ball_last_pixel_index not in (1, config.hitting_range):
            pass
            # FAKE TEST !!! -1
            # assert get_pixel_color(game, ball_last_pixel_index - 1) == Colors.BLACK

        # check ball body
        assert get_pixel_color(game, i) == game.sender.color
        for j in range(ball_last_pixel_index, i):
            assert get_pixel_color(game, j)[0] < 255

        # check ball start, ignoring hitting zone dots
        if i not in (
            config.hitting_range - 2,
            game.receiver.position,
            game.receiver.position - config.hitting_range,
        ):
            assert get_pixel_color(game, i + 1) == Colors.BLACK

    # blue player sends back
    def mock_blue_button_press(self, on_off=None):
        if self.no == config.blue_button_pin:
            return 0

    with mock.patch("machine.Pin.value", mock_blue_button_press):
        run()
    run()

    freezer.tick(game.speed)
    run()
    assert game.balls[0].position == 143

    run()
    assert get_pixel_color(game, 143) == game.receiver.color == game.balls[0].sender.color

    for i in range(142, -1, -1):
        freezer.tick(game.speed)
        run()
        assert game.balls[0].position == i
        run()
        assert get_pixel_color(game, i) == game.receiver.color
        assert get_pixel_color(game, i + 1)[2] < 255

    freezer.tick(game.speed)
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    # lose ball
    freezer.tick(game.speed * 150)
    run()
    assert game.state == game.GETTING_READY

    # skip round animation
    freezer.tick(game.strip.animation.duration_ms * 1000)
    run()
    assert game.strip.animation is None
    assert game.state == game.PLAYING

    # lose ball
    freezer.tick(game.speed * 150)
    run()
    assert game.state == game.ENDING

    # test round animation
    for i in range(1000):
        freezer.tick(1000)
        run()


def test_missed_ball_fading(freezer):
    game = make_playing_game()
    game.bonus_classes = []
    ball = game.balls[0]
    game.pingpong()

    # move ball to just before opponent hitting zone
    target_time = game.speed * (ball.receiver.first_hit_position - ball.position - 1)
    freezer.move_to(time.ticks_us() + target_time)
    game.pingpong()
    game.pingpong()

    # ball is displayed
    assert ball.position == game.strip.num_leds - config.hitting_range - 1
    assert get_pixel_color(game, ball.position) == ball.sender.color
    # just before hitting zone dot
    assert get_pixel_color(game, ball.position + 1) == ball.receiver.color

    ball_position = ball.position

    # receiver presses too soon
    with mock_button_press(ball.receiver):
        game.pingpong()

    assert ball.speed == 0
    assert ball.position == ball_position

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 255)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 50)

    freezer.tick(50000)
    game.pingpong()

    assert ball.speed == 0
    assert ball.position == ball_position

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 242)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 47)

    freezer.tick(50000)
    game.pingpong()

    ball_color_head = get_pixel_color(game, ball.position)
    ball_color_tail = get_pixel_color(game, ball.position - ball.length + 1)
    assert colorsys.rgb_to_hsv(*ball_color_head) == (0.0, 1.0, 229)
    assert colorsys.rgb_to_hsv(*ball_color_tail) == (0.0, 1.0, 44)

    freezer.tick(1000000)
    game.pingpong()
    assert game.balls == []
