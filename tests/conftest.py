import sys

import pytest

from .mocks import machine, micropython, neopixel, time, ucontextlib

sys.modules["machine"] = machine
sys.modules["micropython"] = micropython
sys.modules["time"] = time
sys.modules["ucontextlib"] = ucontextlib
sys.modules["neopixel"] = neopixel
sys.modules["ponguy.config_override"] = None  # type: ignore


@pytest.fixture
def freezer(monkeypatch):
    original_ticks_us = time.ticks_us

    class Freezer:
        microseconds = None

        @classmethod
        def ticks_ms(cls):
            return cls.get_microseconds() // 10**3

        @classmethod
        def ticks_us(cls):
            return cls.get_microseconds()

        @classmethod
        def get_microseconds(cls):
            return cls.microseconds or original_ticks_us()

        @classmethod
        def move_to(cls, microseconds):
            cls.microseconds = microseconds

        @classmethod
        def tick(cls, microseconds):
            cls.microseconds = cls.get_microseconds() + microseconds

    monkeypatch.setattr(time, "ticks_ms", Freezer.ticks_ms)
    monkeypatch.setattr(time, "ticks_us", Freezer.ticks_us)

    return Freezer
