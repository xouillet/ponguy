from contextlib import contextmanager
from unittest import mock

from ponguy import Colors
from ponguy.config import config
from ponguy.game import Game
from ponguy.player import Player
from ponguy.strip import Strip


def make_playing_game(opposite_player_sends=False):
    strip = Strip()
    players = [
        Player(0, 1, Colors.RED, 0, 1),
        Player(
            1,
            2,
            Colors.BLUE,
            config.num_leds - 1,
            -1,
        ),
    ]
    game = Game(strip, players)

    game.sender = players[0] if not opposite_player_sends else players[1]
    game.init_game()
    game.start_pingpong()

    return game


@contextmanager
def mock_button_press(player):
    def mock_value(self, on_off=None):
        if self.no == player.button_pin.no:
            return 0

    with mock.patch("machine.Pin.value", mock_value):
        yield


def get_pixel_color(game, i):
    return game.strip.hw.get_pixel_color(i)


def get_range_color(game, start, end):
    color = get_pixel_color(game, start)

    if any(get_pixel_color(game, i) != color for i in range(start + 1, end)):
        raise ValueError("range contains multiple colors")

    return color
