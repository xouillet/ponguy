# Ponguy

## Hardware info

```mermaid
flowchart TB
    LED["LED strip
         (WS2812B/SK9822)"]
    Mpy{{"`**Ponguy** Micropython MCU
           (RP2040/ESP32)`"}}
    Hupy{{"`**Hupy** Host USB MCU
            (RP2040)`"}}
    Hub["USB Hub"]
    P1[/"`USB controller player 1`"\]
    P2[/"`USB controller player 2`"\]

    Mpy --> LED
    Mpy <-- UART --> Hupy
    Hupy <-- USB-C --> Hub
    Hub <-- USB --> P1
    Hub <-- USB --> P2
```

## Ponguy micropython

### Install micropython

First you will need to install micropython on your MCU, please refer to
https://micropython.org/download/ to download and flash micropython on your device

### Preparing environment

Project metadata and dependencies are handled by `pyproject.toml`.

The following lines describes how to have a working environment with `virtualenv` and `pip`. Feel free
to use your favorite tooling here.

```
virtualenv venv
source venv/bin/activate
pip install -e .
```

### Install code on target

You can upload ponguy code on your MCU via the `upload.sh` script

### Run tests

Tests are written and run via pytest

## Hupy (Host USB for Ponguy)

Hupy is an optional MCU that connects via UART to ponguy MCU. It adds an host USB port that supports
one or more USB controllers (via USB hubs), in order to play ponguy via these controllers.

### Supported controllers

 - [Chupi controller !!](https://git.xouillet.info/xouillet/chupi)
 - [XBox 360 controller](https://en.wikipedia.org/wiki/Xbox_360_controller)
 - [PS2 Wired Buzz! Buzzer](https://en.wikipedia.org/wiki/PlayStation_2_accessories#Buzz!_Buzzer)

### Preparing environment

Hupy need [pico-sdk](https://github.com/raspberrypi/pico-sdk) to build.

 - You can clone it somewhere, then set the `PICO_SDK_PATH` to this directory
 - Or you can run `export PICO_SDK_FETCH_FROM_GIT=1` to do it automatically at first compilation

Pico SDK will need also CMake and arm toolchain. For example on debian/ubuntu

```
sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi libstdc++-arm-none-eabi-newlib
```

### Building

CMake is used for building project

```
cmake -B build .
cd build
make
```

### Uploading

In the `build` directory, you can find `.uf2` files.

A simple way to flash a RPI Pico is to cat `.uf2` file on the block device.

```
cat hupy.uf2 > /dev/sdX
```
 
## Case

A case design in Freecad and basic PCB in Kicad for an ESP32-C3 is provided in the `hw/` folder.

![Case](hw/case.png)
![PCB](hw/pcb.jpg)
